# Introduction 

**Welcome to my Robot Automation Repo.**

# Getting Started

## Dependency’s

First of all you need to check our guide down below:

### Installing Python

*   1.   Open the terminal
*   2.   Run the command `python --version` to check if Python is installed on your OS
*   3.   If it was installed just continue to next step if it is not you need [Install Python](https://realpython.com/installing-python/)
*   4.   Run the command `pip --version` and check if pip is installed on your system.
*   5.   In Linux, sometimes, you need to install pip3, if you run the command above (Step 4) and don't show the pip3 version just do the step bellow, if it returns the pip3 version just go to step 8
*   6.   If python wasn't installed on your OS, you can install by the link (https://www.python.org/downloads/)
*   7.   Once finished the python installation run the command `pip --version` and check that it is installed something like this: `pip 21.2.4 from /usr/lib/python3/dist-packages/pip (python 3.10) 
*   8.   At the end just run the command `git update-index` to update your repository index and the process is done

### Installing VSCode

We recommend you to use the VS Code IDE to code your automation but you can use others. To install VS Code use [This Link](https://code.visualstudio.com/download)

We recommend those plugins:
Robot Framework Intellisense
Robot Framework Intellisense FORK
Robot Extension
Hyper Term Theme

Here you will find a guide to install all the dependency's, lib’s and more to start your project.

Robot Framework Used Libs:

*   robotframework - [Information](https://robotframework.org/)                        
*   robotframework-appiumlibrary - [Keywords](http://serhatbolsu.github.io/robotframework-appiumlibrary/AppiumLibrary.html)              
*   robotframework-jsonlibrary                    
*   robotframework-pythonlibcore                 
*   robotframework-requests - [Keywords](http://marketsquare.github.io/robotframework-requests/doc/RequestsLibrary.html)              
*   robotframework-seleniumlibrary               
*   robotframework-sshlibrary     

### Cloning the Project

After all the dependency's installed and ready you need to clone this project and after you cloned the project you can play with tests.